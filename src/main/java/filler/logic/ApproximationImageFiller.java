package filler.logic;

import filler.data.Image;
import filler.data.Pixel;

import java.util.Set;
import java.util.function.BiFunction;

/**
 * An approximate image filler. Fills fast and loose.
 *
 * @author Aviad
 */
public class ApproximationImageFiller extends ImageFiller {


    /* --- Constructors --- */

    /**
     * Constructs a new ApproximationImageFiller with the given Image.
     *
     * @param image   The image to handle.
     * @param epsilon The epsilon value to use in the default weight function.
     * @param z       The z value to use in the default weight function.
     */
    public ApproximationImageFiller(Image image, float epsilon, float z) {
        super(image, epsilon, z);
    }


    /* --- ImageFiller Impl. --- */

    /**
     * Fixes an image with an approximation algorithm.
     *
     * @see ImageFiller#fixImage()
     */
    public Image fixImage(BiFunction<Pixel, Pixel, Float> weightFunction) {

        Set<Pixel> boundaryPixels = image.getBoundaryPixels();

        while (!image.getInvalidPixels().isEmpty()) {
            Pixel boundaryPixel = boundaryPixels.iterator().next(); // Pick one of the boundary pixels
            Set<Pixel> invalidNeighbours = image.findSpecificNeighbours(boundaryPixel, false); // Find its invalid neighbours
            for (Pixel invalidPixel : invalidNeighbours) { // For each invalid neighbour
                // Fix the value according to its valid (boundary!) neighbours
                fixValue(invalidPixel, image.findSpecificNeighbours(invalidPixel, true), weightFunction);
                image.markAsValid(invalidPixel);
                boundaryPixels.add(invalidPixel);
            }
            boundaryPixels.remove(boundaryPixel); // The boundary pixel is no longer a boundary!
        }

        // No more invalid pixels! hurray!
        return image;
    }

}
