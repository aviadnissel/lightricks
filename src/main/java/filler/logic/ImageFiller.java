package filler.logic;
import filler.data.Image;
import filler.data.Pixel;

import java.util.Set;
import java.util.function.BiFunction;

/**
 * A handler of an image.
 *
 * @author Aviad
 */
public class ImageFiller {


    /* --- Data Members --- */

    /** The image to fill. */
    protected Image image;

    /** The default weight function's epsilon value. */
    private float epsilon;

    /** The default weight function's z value. */
    private float z;


    /* --- Constructors --- */

    /**
     * Constructs a new ImageFiller with the given Image.
     *
     * @param image The image to handle.
     * @param epsilon The epsilon value to use in the default weight function.
     * @param z The z value to use in the default weight function.
     */
    public ImageFiller(Image image, float epsilon, float z) {
        this.image = image;
        this.epsilon = epsilon;
        this.z = z;
    }


    /* --- Public Methods --- */

    /**
     * Fixes the image with the default weight function.
     *
     * @return The fixed image.
     */
    public Image fixImage() {
        return fixImage(this::weight);
    }

    /**
     * Fixes the image with the given weight function.
     *
     * @param weightFunction The weight function to use.
     * @return The fixed Image.
     */
    public Image fixImage(BiFunction<Pixel, Pixel, Float> weightFunction) {

        Set<Pixel> boundaryPixels = image.getBoundaryPixels();
        for(Pixel invalidPixel: image.getInvalidPixels()) {
            fixValue(invalidPixel, boundaryPixels, weightFunction);
        }

        image.clearInvalidPixels();
        return image;
    }


    /* --- Private Methods --- */

    /**
     * A default weight function. Calculates the weighted impact of the boundary pixel on the invalid pixel's fix.
     *
     * @param boundaryPixel The boundary pixel.
     * @param invalidPixel The invalid pixel.
     * @return The weighted value.
     */
    private float weight(Pixel boundaryPixel, Pixel invalidPixel) {
        float pixelDistance = distance(boundaryPixel, invalidPixel);
        return (float) (1 / (Math.pow(pixelDistance, z) + epsilon));
    }

    /**
     * A distance method between two pixels.
     *
     * @param p1 Pixel one.
     * @param p2 Pixel two.
     * @return The distance between the pixels.
     */
    private float distance(Pixel p1, Pixel p2) {
        double xDiff = p1.getX() - p2.getX();
        double yDiff = p1.getY() - p2.getY();
        return (float) Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
    }

    /**
     * Fixes the given invalid pixel according to the boundary pixels.
     * The value is calculated relatively to the given boundary pixels,
     * and the given weight function.
     *
     * @param invalidPixel The invalid pixel to fix.
     * @param boundaryPixels A set of the boundary pixels.
     * @param weightFunction The weight function to use.
     */
    protected void fixValue(Pixel invalidPixel, Set<Pixel> boundaryPixels,
                           BiFunction<Pixel, Pixel, Float> weightFunction) {
        float weightSum = 0;
        float weightValueSum = 0;
        float weightValue;
        for (Pixel boundaryPixel: boundaryPixels) {
            weightValue = weightFunction.apply(invalidPixel, boundaryPixel);
            weightSum += weightValue;
            weightValueSum += (weightValue * boundaryPixel.getValue());
        }
        invalidPixel.setValue(weightValueSum / weightSum);
    }
}
