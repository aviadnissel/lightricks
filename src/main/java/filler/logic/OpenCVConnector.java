package filler.logic;

import filler.data.Image;
import filler.data.Pixel;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.imgcodecs.Imgcodecs.IMREAD_GRAYSCALE;

/**
 * A utility class for handling image loading and saving, using OpenCV.
 *
 * @author Aviad
 */
public class OpenCVConnector {


    /* --- Consts --- */

    /** The dividing factor of byte-to-float-value. */
    private final static int BYTE_TO_FLOAT_VALUE = 255;

    /** The value in the mask picture that marks an invalid pixel. */
    private final static int MASK_INVALID = 0;

    /* --- Static Data Members --- */

    /** Is the class initialized. */
    private static boolean isInitialized = false;


    /* --- Static Public Methods --- */

    /**
     * Loads an image from the given path.
     *
     * @param imagePath The image's path.
     * @param maskPath The path of the mask file, indicating the invalid pixels.
     * @return The loaded image as an Image object.
     */
    public static Image loadImage(String imagePath, String maskPath) throws FileNotFoundException {
        initialize();
        if (!Files.exists(Paths.get(imagePath))) {
            throw new FileNotFoundException(imagePath);
        }
        if (!Files.exists(Paths.get(maskPath))) {
            throw new FileNotFoundException(maskPath);
        }
        Mat imgMat = Imgcodecs.imread(imagePath, IMREAD_GRAYSCALE);
        Mat maskMat = Imgcodecs.imread(maskPath, IMREAD_GRAYSCALE);
        imgMat.convertTo(imgMat, CV_8UC1);

        int height = imgMat.rows();
        int width = imgMat.cols();

        if (height != maskMat.rows() || width != maskMat.cols()) {
            throw new IllegalArgumentException("Image and mask sizes differ!");
        }

        Set<Pixel> invalidPixels = new HashSet<>();
        Pixel[][] image = new Pixel[imgMat.rows()][];
        for (int y = 0; y < height; y++ ) {
            image[y] = new Pixel[width];
            for (int x = 0; x < width; x++) {
                float pixelValue = (float) imgMat.get(y, x)[0];
                float value =  (pixelValue / BYTE_TO_FLOAT_VALUE);
                Pixel pixel = new Pixel(y, x, value);
                image[y][x] = pixel;
                if (maskMat.get(y, x)[0] == MASK_INVALID) {
                    invalidPixels.add(pixel);
                }
            }
        }
        return new Image(image, invalidPixels);
    }

    /**
     * Saves the given image to the given file location.
     *
     * @param image The image to save.
     * @param file The save file's location.
     */
    public static void saveImage(Image image, String file) {
        initialize();
        Pixel[][] pixels = image.getPixels();
        Mat matrix = new Mat(pixels.length, pixels[0].length, CvType.CV_8UC1);
        byte[] data = new byte[pixels.length * pixels[0].length];
        int i = 0;
        for (Pixel[] pixelRow: pixels) {
            for (Pixel pixel: pixelRow) {
                data[i] = (byte) (pixel.getValue() * BYTE_TO_FLOAT_VALUE);
                i += 1;
            }
        }
        matrix.put(0, 0, data);
        Imgcodecs.imwrite(file, matrix);
    }


    /* --- Static Private Methods --- */

    /**
     * Initializes the OpenCV library.
     */
    private static void initialize() {
        if (!isInitialized) {
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
            isInitialized = true;
        }
    }
}
