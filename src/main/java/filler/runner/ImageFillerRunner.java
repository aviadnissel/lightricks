package filler.runner;

import filler.data.Image;
import filler.logic.ApproximationImageFiller;
import filler.logic.ImageFiller;
import filler.logic.OpenCVConnector;

import java.io.FileNotFoundException;

public class ImageFillerRunner {

    public static void main(String[] args) throws FileNotFoundException {

        if (args.length < 5 || args.length > 6) {
            System.err.println("One or more argument missing - " +
                    "<image_file> <mask_file> <output_file> <epsilon> <z> [--fast]");
            System.err.println("(use --fast for faster but less accurate filling)");
        } else {

            String imagePath = args[0];
            String maskPath = args[1];
            String outputPath = args[2];
            float epsilon = new Float(args[3]);
            float z = new Float(args[4]);
            boolean fast = (args.length == 6 && args[5].equals("--fast"));

            Image image = OpenCVConnector.loadImage(imagePath, maskPath);
            ImageFiller filler;
            if (fast) {
                filler = new ApproximationImageFiller(image, epsilon, z);
            } else {
                filler = new ImageFiller(image, epsilon, z);
            }
            Image fixedImage = filler.fixImage();
            OpenCVConnector.saveImage(fixedImage, outputPath);
        }
    }
}
