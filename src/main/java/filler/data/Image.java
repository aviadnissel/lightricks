package filler.data;

import java.util.HashSet;
import java.util.Set;

/**
 * A class representing an image, with invalid pixels in it.
 *
 * @author Aviad
 */
public class Image {


    /* --- Consts --- */

    /** A difference array, used to find the pixel's neighbours. */
    private static final int[] DIFFS = new int[]{-1, 0, 1};


    /* --- Data Members --- */

    /** Image's width. */
    private final int width;

    /** Image's height. */
    private final int height;

    /** The actual array of pixels. */
    private final Pixel[][] pixels;

    /** A set of all the invalid pixels. */
    private Set<Pixel> invalidPixels;


    /* --- Constructor --- */

    /**
     * Creates a new image.
     *
     * @param pixels The pixel array of the image.
     * @param invalidPixels A set of the invalid pixels.
     */
    public Image(Pixel[][] pixels, Set<Pixel> invalidPixels) {
        this.pixels = pixels;
        this.invalidPixels = invalidPixels;
        this.height = pixels.length;
        this.width = pixels[0].length;
    }


    /* --- Getters / Setters --- */

    public Set<Pixel> getInvalidPixels() {
        return invalidPixels;
    }

    public Pixel[][] getPixels() {
        return pixels;
    }


    /* --- Public Methods --- */

    /**
     * Calculates and returns all the boundary pixels in the image.
     *
     * @return A set of the boundary pixels.
     */
    public Set<Pixel> getBoundaryPixels() {

        Set<Pixel> boundaryPixels = new HashSet<>();
        for (Pixel invalidPixel: invalidPixels) {
            boundaryPixels.addAll(findSpecificNeighbours(invalidPixel, true));
        }
        return boundaryPixels;
    }

    /**
     * Clears the list of the invalid pixels.
     */
    public void clearInvalidPixels() {
        invalidPixels.clear();
    }

    /**
     * Finds all the valid or invalid neighbours of the given pixel.
     *
     * @param pixel The pixel we want to find its neighbours of specific type.
     * @param valid True if we're searching for the valid neighbours, false if invalid neighbours.
     * @return A set of valid or invalid neighbours.
     */
    public Set<Pixel> findSpecificNeighbours(Pixel pixel, boolean valid) {
        Set<Pixel> validNeighbours = new HashSet<>();
        Set<Pixel> neighbours = findNeighbours(pixel);
        for (Pixel neighbour: neighbours) {
            boolean isNeighbourValid = !invalidPixels.contains(neighbour);
            if (isNeighbourValid ^ !valid) {
                validNeighbours.add(neighbour);
            }
        }
        return validNeighbours;
    }

    /**
     * Marks the given pixel as valid.
     *
     * @param pixel The pixel to mark.
     */
    public void markAsValid(Pixel pixel) {
        invalidPixels.remove(pixel);
    }


    /* --- Private Methods --- */

    /**
     * Checks if the given point (x and y coordinates) is within the array borders.
     *
     * @param y Y coordinate.
     * @param x X coordinate.
     * @return True or false.
     */
    private boolean isPointInImage(int y, int x) {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    /**
     * Finds all the neighbours of the given pixel.
     *
     * @param pixel The pixel we want to find its neighbours.
     * @return A set of the pixel's neighbours.
     */
    private Set<Pixel> findNeighbours(Pixel pixel) {

        int x = pixel.getX();
        int y = pixel.getY();

        Set<Pixel> neighbours = new HashSet<>();
        for (int yDiff: DIFFS) {
            for (int xDiff : DIFFS) {
                int otherY = y + yDiff;
                int otherX = x + xDiff;
                if (isPointInImage(otherY, otherX)) {
                    if (otherX != x || otherY != y) {
                        neighbours.add(pixels[otherY][otherX]);
                    }
                }
            }
        }
        return neighbours;
    }
}
