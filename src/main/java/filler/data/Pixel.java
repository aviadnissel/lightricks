package filler.data;

/**
 * A pixel in an image.
 *
 * @author Aviad
 */
public class Pixel {


    /* --- Data Members- -- */

    /** X coordinate. */
    private int x;

    /** Y coordinate. */
    private int y;

    /** Value. */
    private float value;


    /* --- Constructors --- */

    /**
     * Constructs a new Pixel with given values.
     *
     * @param y Y coordinate.
     * @param x X coordinate.
     * @param value The pixel's value.
     */
    public Pixel(int y, int x, float value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }

    /**
     * Copy constructor.
     *
     * @param pixel The pixel to copy.
     */
    public Pixel(Pixel pixel) {
        this.x = pixel.getX();
        this.y = pixel.getY();
        this.value = pixel.getValue();
    }


    /* --- Getters / Setters --- */

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    /* --- Object Impl. --- */

    public String toString() {
        return value + "@(" + y + "," + x + ")";
    }

    public boolean equals(Object object) {
        if (!Pixel.class.isInstance(object)) {
            return false;
        }
        Pixel other = (Pixel) object;
        return other.x == x && other.y == y && other.value == value;
    }

}
