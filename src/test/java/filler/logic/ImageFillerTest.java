package filler.logic;

import filler.data.Image;
import filler.data.Pixel;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * A tester for {@link ImageFiller}
 *
 * @author Aviad
 */
public class ImageFillerTest {


    /* --- Tests -- */

    /**
     * Tests a very basic fix.
     */
    @Test
    public void testBasicFix() {
        Pixel[][] pixels = new Pixel[3][];
        pixels[0] = new Pixel[3];
        pixels[1] = new Pixel[3];
        pixels[2] = new Pixel[3];
        pixels[0][0] = new Pixel(0, 0, 1);
        pixels[0][1] = new Pixel(0, 0, 1);
        pixels[0][2] = new Pixel(0, 0, 1);
        pixels[1][0] = new Pixel(1, 0, 1);
        pixels[1][1] = new Pixel(1, 1, 0);
        pixels[1][2] = new Pixel(1, 2, 1);
        pixels[2][0] = new Pixel(2, 0, 1);
        pixels[2][2] = new Pixel(2, 1, 1);
        pixels[2][1] = new Pixel(2, 2, 1);
        Set<Pixel> invalidPixels = new HashSet<>();
        invalidPixels.add(pixels[1][1]);
        Image image = new Image(pixels, invalidPixels);
        ImageFiller filler = new ImageFiller(image, 1, 1);
        filler.fixImage();
        assertEquals(1, pixels[1][1].getValue());

    }

}
