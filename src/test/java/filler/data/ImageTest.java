package filler.data;

import filler.data.Image;
import filler.data.Pixel;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * A tester for {@link Image}.
 *
 * @author Aviad
 */

class ImageTest {


    /* --- Tests --- */

    /**
     * Tests finding boundaries with one row of pixels.
     */
    @Test
    public void testOneRowBoundaries() {
        float[][] imageFloats = {
                {1, -1, 1},
        };
        Pixel[][] pixels = createPixelArray(imageFloats);
        Set<Pixel> invalidPixels = new HashSet<>();
        invalidPixels.add(pixels[0][1]);
        Image image = new Image(pixels, invalidPixels);

        Set<Pixel> boundaryPixels = image.getBoundaryPixels();

        Pixel p00 = new Pixel(0, 0, 1);
        Pixel p02 = new Pixel(0, 2, 1);
        assertTrue(boundaryPixels.stream().anyMatch(x -> x.equals(p00)));
        assertTrue(boundaryPixels.stream().anyMatch(x -> x.equals(p02)));
    }

    /**
     * Tests finding boundaries with two rows of pixels.
     */
    @Test
    public void testTwoRowsBoundaries() {
        float[][] imageFloats = {
                {1, -1, 1},
                {1, 1, 1}
        };

        Pixel[][] pixels = createPixelArray(imageFloats);
        Set<Pixel> invalidPixels = new HashSet<>();
        invalidPixels.add(pixels[0][1]);
        Image image = new Image(pixels, invalidPixels);


        Set<Pixel> boundaryPixels = image.getBoundaryPixels();

        Pixel p00 = new Pixel(0, 0, 1);
        Pixel p02 = new Pixel(0, 2, 1);
        Pixel p10 = new Pixel(1, 0, 1);
        Pixel p11 = new Pixel(1, 1, 1);
        Pixel p12 = new Pixel(1, 2, 1);

        assertTrue(boundaryPixels.stream().anyMatch(x -> x.equals(p00)));
        assertTrue(boundaryPixels.stream().anyMatch(x -> x.equals(p02)));
        assertTrue(boundaryPixels.stream().anyMatch(x -> x.equals(p10)));
        assertTrue(boundaryPixels.stream().anyMatch(x -> x.equals(p11)));
        assertTrue(boundaryPixels.stream().anyMatch(x -> x.equals(p12)));

    }

    /**
     * Tests finding boundaries with no boundaries at all.
     */
    @Test
    public void testNoHole() {
        float[][] imageFloats = {
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}
        };
        Pixel[][] pixels = createPixelArray(imageFloats);
        Set<Pixel> invalidPixels = new HashSet<>();
        Image image = new Image(pixels, invalidPixels);


        Set<Pixel> boundaryPixels = image.getBoundaryPixels();

        assertTrue(boundaryPixels.isEmpty());
    }

    /* --- Private Methods --- */

    private Pixel[][] createPixelArray(float[][] floatsArray) {
        int height = floatsArray.length;
        int width = floatsArray[0].length;
        Pixel[][] pixelsArray = new Pixel[height][];
        for(int y = 0; y < height; y++) {
            pixelsArray[y] = new Pixel[width];
            for(int x = 0; x < width; x++) {
                pixelsArray[y][x] = new Pixel(y, x, floatsArray[y][x]);
            }
        }
        return pixelsArray;
    }
}